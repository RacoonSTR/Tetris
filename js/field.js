$(document).ready(function() {
  var cellSize;
});
/*
* Функция отрисовки ячеек нового блока тетрамино на игровом поле
*/
function addTetramino(cells, x, y) {
  var num = 0;
  for (i = 3; i >= 0; i--) {
    for(j = 0; j < 4; j++) {
      if(cells[i][j] == 1) {
        let cell = "<div class='cell' id='cell" + num + "'></div>";
        $(".field").append(cell);
        $("#cell" + num).css("top", ((x + i) * field.cellSize) + "px");
        $("#cell" + num).css("left",((y + j) * field.cellSize) + "px");
        num++;
      }
    }
  }
}

/*
* Функция удаления ячеек блока тетрамино
*/
function deleteTetramino() {
  $(".cell[id]:not(.stopped)").remove();
}

/*
* Функция отрисовки ячеек нового блока тетрамино на поле следующего тетрамино
*/
function addNextTetramino(cells) {
  $(".next_block_field").empty();
  var num = 0;
  for (i = 3; i >= 0; i--) {
    for(j = 0; j < 4; j++) {
      if(cells[i][j] != 0) {
        let cell = "<div class='cell stopped' id='next_cell" + num + "'></div>";
        $(".next_block_field").append(cell);
        field.cellSize = $("div.cell").outerHeight();
        $("#next_cell" + num).css("top", ((1 + i) * field.cellSize) + "px");
        $("#next_cell" + num).css("left",((1 + j) * field.cellSize) + "px");
        num++;
      }
    }
  }
}


/*
* Функция, присваивающая ячейкам тетраминокласс .stopped, что означает
* остановленные ячейки на игровом поле
*/
function stopTetramino() {
  $(".cell").removeAttr("id");
  $(".cell").addClass('stopped');
}

/*
* Функция, анимированно опускающая ячейки блока тетрамино вниз
*/
function turnCellsDown(time) {
  $(".cell[id]:not(.stopped)").animate(
    {"top": "+=" + field.cellSize + "px"}, time
  );
}

/*
* Функция, анимированно опускающая ячейки блока тетрамино вниз на определенное
* расстояние
*/
function turnCellsDownFast(count) {
  $(".cell[id]:not(.stopped)").animate(
    {"top": "+=" + (field.cellSize * count) + "px"}, 0
  );
}

/*
* Функция, анимированно опускающая ячейки блока тетрамино влево
*/
function turnCellsLeft() {
  $(".cell[id]:not(.stopped)").animate(
    {"left": "-=" + field.cellSize + "px"}, 100
  );
}

/*
* Функция, анимированно опускающая ячейки блока тетрамино вправо
*/
function turnCellsRight() {
  $(".cell[id]:not(.stopped)").animate(
    {"left": "+=" + field.cellSize + "px"}, 100
  );
}

/*
* Функция, удаляющая ячейки полной строки игрового поля и опускающая вниз
* ячейки, находящиеся выже этой строки
*/
function deleteRow(i) {
  $(".cell").each(function(index) {
    if(parseInt($(this).css("top")) <= i * field.cellSize) {
      if(parseInt($(this).css("top")) > (i - 1) * field.cellSize) {
        $(this).remove();
      }
    }
  });
  $(".cell").each(function(index)  {
    if(parseInt($(this).css("top")) < i * field.cellSize) {
      $(this).offset(function(i,val){
    	  return {top:val.top + field.cellSize, left:val.left};
    	});
    }
  });
}

/*
* Функция, очищающая поле
*/
function cleanField() {
  $(".field").empty();
}
/*
* Функция изменения отображаемого игрового счета
*/
function changeScore(score) {
  $('.score_field h2').html(score.toString());
}

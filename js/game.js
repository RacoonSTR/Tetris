/*
* Класс блока тетрамино
*/
class tetraminoBlock {
  constructor() {
    this.cells = [[0, 0, 0, 0],[0, 0, 0, 0],[0, 0, 0, 0],[0, 0, 0, 0]];
    this.x = 0;
    this.y = 0;
    this.stopped = true;
  }

}

/*
* Класс игры, содержащий матрицу игры и функции, осуществляющие игровой процесс
*/
class tetris {
  constructor() {
    this.matrix = [];
    this.score = 0;
    this.currentBlock = new tetraminoBlock();
    this.nextBlock = new tetraminoBlock();
    this.timer;
    this.stopped = false;
    this.currentBlockVariants = [
      [[0, 0, 0, 0],[1, 1, 1, 1],[0, 0, 0, 0],[0, 0, 0, 0]],
      [[0, 0, 0, 0],[1, 1, 1, 0],[1, 0, 0, 0],[0, 0, 0, 0]],
      [[0, 0, 0, 0],[1, 1, 1, 0],[0, 1, 0, 0],[0, 0, 0, 0]],
      [[0, 0, 0, 0],[1, 1, 1, 0],[0, 0, 1, 0],[0, 0, 0, 0]],
      [[0, 0, 0, 0],[1, 1, 0, 0],[0, 1, 1, 0],[0, 0, 0, 0]],
      [[0, 0, 0, 0],[0, 1, 1, 0],[1, 1, 0, 0],[0, 0, 0, 0]],
      [[0, 0, 0, 0],[0, 1, 1, 0],[0, 1, 1, 0],[0, 0, 0, 0]]
    ];
  }

  /*
  * Инициализация матрицы в начале игры
  */
  initializeMatrix() {
    for (let i = 0; i < 20; i++) {
      if (this.matrix[i] == undefined) {
        this.matrix[i] = [];
      }
      for(let j = 0; j < 10; j++) {
        this.matrix[i][j] = 0;
      }
    }
    this.matrix[20] = [];
    for(let j = 0; j < 10; j++) {
      this.matrix[20][j] = 1;
    }
  }

  /*
  * функция, которая создает новый блок
  */
  addNewBlock() {
    let rand = Math.floor(Math.random() * this.currentBlockVariants.length);
    let block = new tetraminoBlock();
    block.cells = this.currentBlockVariants[rand];
    return block;
  }

  /*
  * функция, которая проверяет, возможно ли вставить блок на игровое поле, и
  * вставляет его в игровое поле, если возможно
  */
  addBlockToField(block) {
    let rowCount = block.cells.length;
    let columnCount = block.cells[0].length;
    for (let i = 0; i < rowCount; i++) {
      for (let j = 0; j < columnCount; j++) {
        if (block.cells[i][j] == 1) {
          if (this.matrix[i - 1][3 + j] == 1) {
            this.stopped = true;
            return;
          }
        }
      }
    }
    block.x = -1;
    block.y = 3;
    block.stopped = false;
    this.block = block;
    addTetramino(block.cells, -1, 3);
  }

  /*
  * Функция, проверяющая возможность сдвига блока тетрамино влево и реализующая
  * сдвиг в случае возможности
  */
  moveBlockLeft() {
    let rowCount = this.currentBlock.cells.length;
    let columnCount = this.currentBlock.cells[0].length;
    for (let i = rowCount - 1; i >= 0; i--) {
      for (let j = 0; j < columnCount; j++) {
        if (this.currentBlock.cells[i][j] != 0) {
          let x = this.currentBlock.x + i;
          let y = this.currentBlock.y + j;
          if(this.matrix[x][y - 1] == 0) {
            break;
          }
          else {
            return;
          }
        }
      }
    }
    this.currentBlock.y--;
    turnCellsLeft();
    return;
  }

  /*
  * Функция, проверяющая возможность сдвига блока тетрамино вправо и реализующая
  * сдвиг в случае возможности
  */
  moveBlockRight() {
    let rowCount = this.currentBlock.cells.length;
    let columnCount = this.currentBlock.cells[0].length;
    for (let i = rowCount - 1; i >= 0; i--) {
      for (let j = columnCount - 1; j >= 0; j--) {
        if (this.currentBlock.cells[i][j] != 0) {
          let x = this.currentBlock.x + i;
          let y = this.currentBlock.y + j;
          if(this.matrix[x][y + 1] == 0) {
            break;
          }
          else {
            return;
          }
        }
      }
    }
    this.currentBlock.y++;
    turnCellsRight();
    return;

  }

  /*
  * Функция, проверяющая возможность сдвига блока тетрамино вниз и реализующая
  * сдвиг в случае возможности
  */
  moveBlockDown() {
    let rowCount = this.currentBlock.cells.length;
    let columnCount = this.currentBlock.cells[0].length;
    for (let i = rowCount-1; i >= 0; i--) {
      for (let j = 0; j < columnCount; j++) {
        if (this.currentBlock.cells[i][j] != 0) {
          let x = this.currentBlock.x + i;
          let y = this.currentBlock.y + j;
          if(this.matrix[x + 1][y] == 1) {
            this.currentBlock.stopped = true;
            return false;
          }
        }
      }
    }
    this.currentBlock.x++;
    return true;
  }

  /*
  * Функция, вызываемая при нажатии кнопки "ВНИЗ" и опускающая блок тетрамино
  * сразу вниз
  */
  moveBlockDownFast(callback) {
    let count = 0;
    while (this.moveBlockDown()) count++;
    turnCellsDownFast(count);
    callback();
  }

  /*
  * Функция, которая проверяет возможность поворота блока тетрамино по часовой
  * стрелке, и осуществляет этот поворот (не доделано)
  */
  turnBlock() {
    let rowCount = this.currentBlock.cells.length;
    let columnCount = this.currentBlock.cells[0].length;
    let turnedBlock = [[0, 0, 0, 0],[0, 0, 0, 0],[0, 0, 0, 0],[0, 0, 0, 0]];
    for (let i = 0; i < rowCount; i++) {
      for (let j = 0; j < columnCount; j++) {
        turnedBlock[i][j] = this.currentBlock.cells[j][4-i-1];
      }
    }
    for (i = 0; i < rowCount; i++) {
      for (j = 0; j < columnCount; j++) {
        if (turnedBlock[i][j] == 1) {
          let x = this.currentBlock.x + i;
          let y = this.currentBlock.y + j;
          if ((this.matrix[x] == undefined) || (this.matrix[x][y] != 0)) {
            return;
          }
        }
      }
    }
    this.currentBlock.cells = turnedBlock;
    deleteTetramino();
    addTetramino(
      this.currentBlock.cells,
      this.currentBlock.x,
      this.currentBlock.y
    );
    return;
  }

  /*
  * Функция, которая останавливает блок тетрамино и изменяет матрицу игры
  */
  stopBlock() {
    let rowCount = this.currentBlock.cells.length;
    let columnCount = this.currentBlock.cells[0].length;
    for (let i = rowCount - 1; i >= 0; i--) {
      for (let j = 0; j < columnCount; j++) {
        let x = this.currentBlock.x;
        let y = this.currentBlock.y;
        if (((x + i) >= 0) && ((x + i) < 20)) {
          if (((y + j) >= 0) && ((y + j) < 10)) {
            this.matrix[x + i][y + j] += this.currentBlock.cells[i][j];
          }
        }
      }
    }
    stopTetramino();
  }

  /*
  * Функция, которая проверяет строки поля на заполненность и удаляет
  * заполненные
  */
  checkFullRow() {
    let i = 19;
    while (i > 0) {
      let isFull = this.matrix[i].every(function(number) {
          return (number == 1);
        });
      if(isFull) {
        this.matrix.splice(i, 1);
        this.matrix.unshift([]);
        for (j = 0; j < 10; j++) {
          this.matrix[0][j] = 0;
          this.matrix[0].length = 10;
        }
        this.score += 100;
        changeScore(this.score);
        deleteRow(i);
      }
      else {
        i--;
      }
    }
  }
}

/*
* При загрузке документа инициализируем новую игру
*/

$(document).ready(function() {
  var game = new tetris();
  game.initializeMatrix();
  game.nextBlock = game.addNewBlock();
  addNextTetramino(game.nextBlock.cells);
  /*
  * Реализуем управление с клавиатуры
  */
  var keyboardLock = false;
  $(document).keyup(function(e) {
    if (!keyboardLock) {
      if(e.which == 37) {
        game.moveBlockLeft();
      }
      else if (e.which == 39) {
        game.moveBlockRight();
      }
      else if (e.which == 40) {
        keyboardLock = true;
        game.moveBlockDownFast(function () {
          keyboardLock = false;
        });
      }
      else if (e.which == 38) {
        game.turnBlock();
      }
    }
  });
  /*
  * Игровой таймер, ответственный за изменения на игровом поле
  */
  game.timer = setTimeout(function run() {
    if(!game.stopped) {
      if (!game.currentBlock.stopped) {
        if(game.moveBlockDown()) {
          turnCellsDown(500);
        }
      }
      else {
        game.stopBlock();
        game.checkFullRow();
        game.currentBlock = game.nextBlock;
        game.nextBlock = game.addNewBlock();
        addNextTetramino(game.nextBlock.cells);
        game.addBlockToField(game.currentBlock);
      }
      setTimeout(run, 1000);
    }
  }, 1000);
});
